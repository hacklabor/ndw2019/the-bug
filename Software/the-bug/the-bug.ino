/*
  Fade

  This example shows how to fade an LED on pin 9 using the analogWrite()
  function.

  The analogWrite() function uses PWM, so if you want to change the pin you're
  using, be sure to use another PWM capable pin. On most Arduino, the PWM pins
  are identified with a "~" sign, like ~3, ~5, ~6, ~9, ~10 and ~11.

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Fade
*/

const int pinEye1 = A5;  
const int pinEye2 = A6;
const int analogInPin = A2;


bool touch = false;
bool touchMerker = false;
byte ProgNr = 5;
byte maxProgNr = 10;
// the setup routine runs once when you press reset:
void AUS(){
   digitalWrite(pinEye1, HIGH);
     digitalWrite(pinEye2, HIGH);
  }

  void EIN(){
   digitalWrite(pinEye1, LOW);
     digitalWrite(pinEye2, LOW);
  }
void getTouch (){
   int value = analogRead(analogInPin);
  
 
  if (value>800){
   
      ProgNr++;
      if (ProgNr>maxProgNr){ProgNr=1;}
      AUS();
     while(analogRead(analogInPin)>750){
     digitalWrite(pinEye1, LOW);
     delay(250);
     digitalWrite(pinEye1, HIGH);
     delay(250);
    }
    for (int i=1;i<ProgNr+1;i++){
      digitalWrite(pinEye2, LOW);
     delay(250);
     digitalWrite(pinEye2, HIGH);
     delay(250);
    }
    
}
}
void wechselBlinken(int flashSpeed){
   digitalWrite(pinEye1, LOW);
      digitalWrite(pinEye2, HIGH);
    delay(flashSpeed);
     digitalWrite(pinEye1, HIGH);
     digitalWrite(pinEye2, LOW);
       delay(flashSpeed);
}
void gleichBlinken(int flashSpeed){
      EIN();
    delay(flashSpeed);
      AUS();
       delay(flashSpeed);
}

   void randomFlash(int Prozent){
    
    if (random(100)>Prozent){return;}
     EIN();
     delay(10);
     AUS();
  }
  void randomFlashSeperat(int Prozent){
    
    if (random(100)<Prozent){digitalWrite(pinEye1, LOW);}
     if (random(100)<Prozent){digitalWrite(pinEye2, LOW);}
     delay(10);
     AUS();
  }

void setup() {
  // declare pin 9 to be an output:
  pinMode(pinEye1, OUTPUT);
   pinMode(pinEye2, OUTPUT);
    //analogReference(INTERNAL);
    for (int i=0; i<5; i++){
      digitalWrite(pinEye1, LOW);
      digitalWrite(pinEye2, HIGH);
    delay(100);
     digitalWrite(pinEye1, HIGH);
     digitalWrite(pinEye2, LOW);
       delay(100);
    }
    
     
}

// the loop routine runs over and over again forever:
void loop() {
 
  getTouch();
switch (ProgNr) {
   case 1:
    // Statement(s)
    AUS();
    break;
     case 2:
    // Statement(s)
    EIN();
    break;
     case 3:
    // Statement(s)
   randomFlash(15);
    break;
     case 4:
    // Statement(s)
   randomFlash(55);
    break;
      case 5:
    // Statement(s)
   randomFlashSeperat(15);
   break;
     case 6:
    // Statement(s)
   randomFlashSeperat(55);
   break;
  case 7:
    // Statement(s)
    wechselBlinken(50);
    break;
  case 8:
    // Statement(s)
    wechselBlinken(150);
    break;
   case 9:
    // Statement(s)
    gleichBlinken(80);
    break;
     case 10:
    // Statement(s)
    gleichBlinken(150);
    break;
   
    
    
}

  
  delay(10);
  
}
